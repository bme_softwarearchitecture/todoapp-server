using System.Data.Entity;

namespace ErettsegiApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class parentid : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Folder", "ParentId", "dbo.Folder");
            DropIndex("dbo.Folder", new[] { "ParentId" });
            AlterColumn("dbo.Folder", "ParentId", c => c.Int());
            CreateIndex("dbo.Folder", "ParentId");
            AddForeignKey("dbo.Folder", "ParentId", "dbo.Folder", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Folder", "ParentId", "dbo.Folder");
            DropIndex("dbo.Folder", new[] { "ParentId" });
            AlterColumn("dbo.Folder", "ParentId", c => c.Int(nullable: false));
            CreateIndex("dbo.Folder", "ParentId");
            AddForeignKey("dbo.Folder", "ParentId", "dbo.Folder", "Id");
        }

      

      
    }
}
