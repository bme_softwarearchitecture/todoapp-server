namespace ErettsegiApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class rootfolder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "RootFolderId", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "RootFolderId");
        }
    }
}
