﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using TODOApp.DTO;
using TODOApp.Models;

namespace TODOApp.Repository
{
  public class Repository<TObject> : IRepository<TObject>

        where TObject : class
  {
    protected ApplicationDbContext Context;
    private bool shareContext = false;

    public Repository()
    {
      Context = new ApplicationDbContext();
    }

    public Repository(ApplicationDbContext context)
    {
      Context = context;
    }

    protected DbSet<TObject> DbSet
    {
      get
      {
        return Context.Set<TObject>();
      }
    }

    public void Dispose()
    {
      if (shareContext && (Context != null))
        Context.Dispose();
    }

    public virtual IQueryable<TObject> All()
    {
      return DbSet.AsQueryable();
    }

    public virtual List<TObject>
Filter(Expression<Func<TObject, bool>> predicate)
    {
      return DbSet.Where(predicate).AsQueryable<TObject>().ToList();
    }


    public IQueryable<TObject> GetQueryable(SOBase<TObject> soBase)
    {
      var resetSet = soBase.SearchFilter != null ? DbSet.Where(soBase.SearchFilter).AsQueryable() : DbSet.AsQueryable();
      return resetSet;
    }

    

    public virtual List<TObject> Filter(SOBase<TObject> soBase)
    {

      if (soBase.PageNumber != 0)
      {
        soBase.PageNumber--;        
      }

      if (soBase.DisablePageing)
      {
        var result = soBase.SearchFilter != null ? DbSet.Where(soBase.SearchFilter).AsQueryable() : DbSet.AsQueryable();

        soBase.PageCount = 1;

        result = soBase.OrderFilter;

        return result.ToList();
      }
      else
      {
        int skipCount = soBase.PageNumber * soBase.PageSize;
        var result = soBase.SearchFilter != null ? DbSet.Where(soBase.SearchFilter).AsQueryable() : DbSet.AsQueryable();

        soBase.PageCount = (int)Math.Ceiling(result.Count() / (double)soBase.PageSize);

        result = soBase.OrderFilter.Skip(skipCount).Take(soBase.PageSize);

        return result.ToList();  
      }

      
    }




    public bool Contains(Expression<Func<TObject, bool>> predicate)
    {
      return DbSet.Count(predicate) > 0;
    }

   


    public virtual TObject Find(Expression<Func<TObject, bool>> predicate)
    {
      return DbSet.FirstOrDefault(predicate);
    }

    public virtual TObject Create(TObject TObject)
    {
      var newEntry = DbSet.Add(TObject);
      if (!shareContext)
        try
        {
          Context.SaveChanges();
        }
        catch (DbEntityValidationException e)
        {
          var newException = e;
          throw newException;
        }
        
      return newEntry;
    }

    

    int IRepository<TObject>.Count(Expression<Func<TObject, bool>> predicate)
    {
      return DbSet.Where(predicate).Count();
    }
    
    public virtual void Delete(TObject TObject)
    {
      DbSet.Remove(TObject);
      if (!shareContext)
      Context.SaveChanges();
      return;
    }

    public virtual TObject Update(TObject TObject, Func<TObject, bool> getKey)
    {
      var entry = Context.Entry(TObject);


     if (entry.State == EntityState.Detached) {
    var set = Context.Set<TObject>();
    TObject attachedEntity = set.Local.SingleOrDefault(getKey); 

    if (attachedEntity != null) {
        var attachedEntry = Context.Entry(attachedEntity);
        attachedEntry.CurrentValues.SetValues(TObject);
    } else {
        entry.State = EntityState.Modified; 
    }
}

      if (!shareContext)
      {
        Context.SaveChanges();
        return TObject;
      }
      return null;
    }

    public virtual int Delete(Expression<Func<TObject, bool>> predicate)
    {
      var objects = Filter(predicate);
      foreach (var obj in objects)
        DbSet.Remove(obj);
      if (!shareContext)
        return Context.SaveChanges();
      return 0;
    }
  }
}