﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;
using TODOApp.Models;

namespace TODOApp.Repository
{
  public interface IUserLoginRepository : IRepository<IdentityUserLogin>
  {
    
  }
  public class UserLoginRepository : Repository<IdentityUserLogin>, IUserLoginRepository
  {
    public UserLoginRepository(ApplicationDbContext context) : base(context) { }    
  }

}