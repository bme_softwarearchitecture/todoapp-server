﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Web;
using Microsoft.AspNet.Identity;
using TODOApp.Models;

namespace TODOApp.Repository
{
  public interface IUnitOfWork : IDisposable
  {
    void SaveChanges();
  }

  public interface IDBContext : IUnitOfWork
  {
    IFolderRepository FolderRepository { get; }
    INoteRepository NoteRepository { get;  }

    IApplicationUserRepository ApplicationUser { get; }

    IUserLoginRepository UserLoginRepository { get; }
    
    void Reload(Object o);
  }


  public class DBContext : IDBContext
  {
    private ApplicationDbContext dbContext;
    private IApplicationUserRepository applicationUser;
    private INoteRepository note;
    private IFolderRepository folder;
    private IUserLoginRepository userLogin;

    public DBContext()
    {
      dbContext = new ApplicationDbContext();
      
    }
  
    public INoteRepository NoteRepository
    {
      get
      {
        if (note==null)
          note = new NoteRepository(dbContext);
        return note;
      }
    }

    public IUserLoginRepository UserLoginRepository
    {
      get
      {
        if (userLogin == null)
          userLogin = new UserLoginRepository(dbContext);
        return userLogin;
      }
    }


    public IFolderRepository FolderRepository
    {
      get
      {
        if (folder==null) 
          folder = new FolderRepository(dbContext);
        return folder;
      }
    }

    public IApplicationUserRepository ApplicationUser
    {
      get
      {
        if (applicationUser==null)
          applicationUser = new ApplicationUserRepository(dbContext);
        return applicationUser;
      }
    }


    public void Reload(Object o)
    {
      dbContext.Entry(o).Reload();
    }

    public void SaveChanges()
    {
      dbContext.SaveChanges();
    }

    public void Dispose()
    {
      if (userLogin!=null)
        userLogin.Dispose();
      if (note!=null)
        note.Dispose();
      if (folder!=null)
        folder.Dispose();
      if (applicationUser!=null)
        applicationUser.Dispose();
      if (dbContext != null)
        dbContext.Dispose();
      GC.SuppressFinalize(this);
    }
  }
}