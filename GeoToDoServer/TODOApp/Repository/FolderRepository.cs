﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using TODOApp.Models;

namespace TODOApp.Repository
{
  public interface IFolderRepository : IRepository<Folder>
  {
    
  }
  public class FolderRepository : Repository<Folder>, IFolderRepository
  {
    public FolderRepository(ApplicationDbContext context) : base(context) { }    
  }

}