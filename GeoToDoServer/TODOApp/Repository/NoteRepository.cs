﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using TODOApp.Models;

namespace TODOApp.Repository
{
  public interface INoteRepository : IRepository<Note>
  {
    
  }
  public class NoteRepository : Repository<Note>, INoteRepository
  {
    public NoteRepository(ApplicationDbContext context) : base(context) { }    
  }

}