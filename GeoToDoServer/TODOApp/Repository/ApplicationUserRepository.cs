﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using TODOApp.Models;

namespace TODOApp.Repository
{
  public interface IApplicationUserRepository : IRepository<ApplicationUser>
  {
    
  }
  public class ApplicationUserRepository : Repository<ApplicationUser>, IApplicationUserRepository
  {
    public ApplicationUserRepository(ApplicationDbContext context) : base(context) { }    
  }

}