﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TODOApp.DTO;

namespace TODOApp.Repository
{
  public interface IRepository<T> : IDisposable where T : class
  {
    /// <summary>
    /// Gets all objects from database
    /// </summary>
    IQueryable<T> All();

    /// <summary>
    /// Gets objects from database by filter.
    /// </summary>
    /// <param name="predicate">Specified a filter</param>
    List<T> Filter(Expression<Func<T, bool>> predicate);

    /// <summary>
    /// Gets objects from database with filting and paging.
    /// </summary>
    /// <typeparam name="Key"></typeparam>
    /// <param name="filter">Specified a filter</param>
    /// <param name="total">Returns the total records count of the filter.</param>
    /// <param name="index">Specified the page index.</param>
    /// <param name="size">Specified the page size</param>
    List<T> Filter(SOBase<T> soBase);

    /// <summary>
    /// Gets the object(s) is exists in database by specified filter.
    /// </summary>
    /// <param name="predicate">Specified the filter expression</param>
    bool Contains(Expression<Func<T, bool>> predicate);

   
   

    /// <summary>
    /// Find object by specified expression.
    /// </summary>
    /// <param name="predicate"></param>
    T Find(Expression<Func<T, bool>> predicate);

    /// <summary>
    /// Create a new object to database.
    /// </summary>
    /// <param name="t">Specified a new object to create.</param>
    T Create(T t);

    /// <summary>
    /// Delete the object from database.
    /// </summary>
    /// <param name="t">Specified a existing object to delete.</param>        
    void Delete(T t);

    /// <summary>
    /// Delete objects from database by specified filter expression.
    /// </summary>
    /// <param name="predicate"></param>
    int Delete(Expression<Func<T, bool>> predicate);

    /// <summary>
    /// Update object changes and save to database.
    /// </summary>
    /// <param name="t">Specified the object to save.</param>
    T Update(T t, Func<T, bool> getKey);

   
    IQueryable<T> GetQueryable(SOBase<T> soBase);

    int Count(Expression<Func<T, bool>> predicate);
  }
}
