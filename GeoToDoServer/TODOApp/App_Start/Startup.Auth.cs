﻿using System.Security.Claims;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.Google;
using Owin;

namespace TODOApp
{
  public partial class Startup
  {
    // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
    public void ConfigureAuth(IAppBuilder app)
    {
      // Enable the application to use a cookie to store information for the signed in user
      app.UseCookieAuthentication(new CookieAuthenticationOptions
      {
        AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
        LoginPath = new PathString("/Account/Login")
      });
      // Use a cookie to temporarily store information about a user logging in with a third party login provider
      app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

      // Uncomment the following lines to enable logging in with third party login providers
      //app.UseMicrosoftAccountAuthentication(
      //    clientId: "",
      //    clientSecret: "");

      //app.UseTwitterAuthentication(
      //   consumerKey: "",
      //   consumerSecret: "");


      //var facebookAuthenticationOptions = new FacebookAuthenticationOptions
      //{
      //  AppId = "498122140291101",
      //  AppSecret = "102fc64cf4e403141ebda2c8224893af"//,
      //  //Provider = new FacebookAuthenticationProvider()
      //  //{
      //  //  OnAuthenticated = async context =>
      //  //  {
      //  //    context.Identity.AddClaim(new Claim("picture", context.User.GetValue("picture").ToString()));
      //  //    context.Identity.AddClaim(new Claim("profile", context.User.GetValue("profile").ToString()));
      //  //  }
      //  //}
      //};
      //facebookAuthenticationOptions.Scope.Add("email");
      //facebookAuthenticationOptions.Scope.Add("public_profile");
      //app.UseFacebookAuthentication(facebookAuthenticationOptions);


      //var googleOAuth2AuthenticationOptions = new GoogleOAuth2AuthenticationOptions
      //{
      //  ClientId = "27065584314-0s0fs3ta3u41uoacpce1mogob7sr4hk5.apps.googleusercontent.com",
      //  ClientSecret = "UeAcpkAxXzYboDdnGfyYHqxJ",
      //  //CallbackPath = new PathString("/Account/ExternalGoogleLoginCallback"),
      //  Provider = new GoogleOAuth2AuthenticationProvider()
      //  {
      //    OnAuthenticated = async context =>
      //    {
      //      context.Identity.AddClaim(new Claim("picture", context.User.GetValue("picture").ToString()));
      //      context.Identity.AddClaim(new Claim("profile", context.User.GetValue("profile").ToString()));
      //    }
      //  }
      //};

      //googleOAuth2AuthenticationOptions.Scope.Add("email");

      //app.UseGoogleAuthentication(googleOAuth2AuthenticationOptions);

      //app.UseGoogleAuthentication(
      //   clientId: "27065584314-0s0fs3ta3u41uoacpce1mogob7sr4hk5.apps.googleusercontent.com",
      //   clientSecret: "UeAcpkAxXzYboDdnGfyYHqxJ");
      

    }
  }
}