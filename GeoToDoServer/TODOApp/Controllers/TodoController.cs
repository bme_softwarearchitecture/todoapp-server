﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Web.Mvc;
using ErettsegiApp.DTO;
using Microsoft.AspNet.Identity;
using TODOApp.DTO;
using TODOApp.Models;
using TODOApp.Service;
using TODOApp.Utilities;

namespace TODOApp.Controllers
{
  public class TodoController : Controller
  {
    private readonly ITodoService _todoService;

    public TodoController(ITodoService todoService)
    {
      _todoService = todoService;
    }

   
    [System.Web.Http.HttpPost]
    [AuthorizeTablet]
    public ActionResult GetTodos(int? parentId,int? id, string searchText, DateTime? lastUpdate, int? pageNumber, int? pageSize)
    {
      var userId = HttpContext.Items["userId"] as string;

      SONote soNote = new SONote()
      {
        ParentId = parentId,
        NoteId = id,
        SearchText = searchText,
        UserId = userId,
        LasUpdate = lastUpdate
      };

      SOFolder soFolder = new SOFolder()
      {
        ParentId = parentId,
        Id = id,
        Name = searchText,
        UserId = userId,
        LasUpdate = lastUpdate
      };

      if (pageNumber == null || pageSize == null)
      {
        soNote.DisablePageing = true;
        soFolder.DisablePageing = true;
      }
      else
      {
        soNote.PageNumber = pageNumber.GetValueOrDefault(1);
        soNote.PageSize = pageSize.GetValueOrDefault(10);
        soFolder.PageNumber = soNote.PageNumber;
        soFolder.PageSize = soNote.PageSize;
      }

      OperationResult<DTOTodo> result = new OperationResult<DTOTodo>();
      result.Output = new DTOTodo();
      result.Output.Folders = _todoService.GetFolders(soFolder).SearchResult;
      result.Output.Notes = _todoService.GetNotes(soNote).SearchResult;

      return result.ToJsonNetResult();

    }

    [System.Web.Http.HttpPost]
    [AuthorizeTablet]
    public ActionResult SaveTodos(List<DTONote> notes,List<DTOFolder> folders, string deviceId)
    {
      var userId = HttpContext.Items["userId"] as string;

      OperationResult<DTOTodo> result = new OperationResult<DTOTodo>();
      result.Output = _todoService.SaveTodos(new DTOTodo()
      {
        Notes = notes,
        Folders = folders
      },userId,deviceId).Output;

      return result.ToJsonNetResult();
    }

   
  }
}