﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography.X509Certificates;
using System.Web.Mvc;
using TODOApp.DTO;
using TODOApp.Models;
using TODOApp.Service;
using Facebook;
using Microsoft.AspNet.Identity;

namespace TODOApp.Controllers
{
  public class HomeController : Controller
  {
    public ActionResult Index()
    {
      return View();
    }

  }
}