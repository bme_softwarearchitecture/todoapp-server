﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AutoMapper;
using TODOApp.DTO;
using TODOApp.Repository;
using TODOApp.Utilities;
using Facebook;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using TODOApp.Models;
using TODOApp.Service;

namespace TODOApp.Controllers
{
  
  public class AccountController : Controller
  {
    private readonly IAccountService _accountService;



    public AccountController(IAccountService accountService)
    {
      _accountService = accountService;
      UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
      UserManager = userManager;
      UserManager.UserValidator = new UserValidator<ApplicationUser>(UserManager)
      {
        AllowOnlyAlphanumericUserNames = false
      };
    }

    public UserManager<ApplicationUser> UserManager { get; private set; }




    [System.Web.Http.HttpPost]
    [AllowAnonymous]
    public ActionResult Login(string userName, string password)
    {
      var result = new OperationResult<TabletLogin>();
      ApplicationUser user = UserManager.Find(userName, password);
      if (user != null)
      {
        var t = TokenGenerator.GenerateToken(user.Id, user.PasswordHash, null);
        result.Output = new TabletLogin()
        {
          Token = t,
          UserName = userName
        };
        return result.ToJsonNetResult();
      }

      result.Errors.Add(new ValidationError("Hibás jelszó vagy felhasználónév", 1));
      Response.StatusCode = 403;
      return result.ToJsonNetResult();
    }


    [HttpPost]
    [AuthorizeTablet]
    public ActionResult ChangePassword(string newPassword, string oldPassword)
    {
      var userId = HttpContext.Items["userId"] as string;
      var res = new OperationResult<TabletLogin>();
      if (ModelState.IsValid)
      {
        IdentityResult change = UserManager.ChangePassword(userId, oldPassword, newPassword);

        if (change.Succeeded)
        {
          var user = _accountService.GetApplicationUser(userId).Output;
          var t = TokenGenerator.GenerateToken(userId, user.PasswordHash, null);

          var r = new TabletLogin()
          {
            Token = t,
            UserName = user.UserName
          };
          res.Output = r;
          return res.ToJsonNetResult();
        }

        var error = change.Errors.FirstOrDefault();
        res.Errors.Add(new ValidationError(error, 5));
      }


      Response.StatusCode = 403;
      return res.ToJsonNetResult();
    }


    [HttpPost]
    [AuthorizeTablet]
    public ActionResult UpdateDeviceId(string newDeviceId, string oldDeviceId)
    {
      var userId = HttpContext.Items["userId"] as string;
      _accountService.UpdateDeviceId(userId,oldDeviceId,newDeviceId);
      
      return (new EmptyResult());
    }
    
    [HttpPost]
    [AllowAnonymous]
    public ActionResult PasswordRecovery(string  userName)
    {

      OperationResult result = new OperationResult();
      using (var context = new ApplicationDbContext())
      {
        ApplicationUser user = context.Users.FirstOrDefault(a => a.UserName == userName);
        if (user != null)
        {
          string newPassword = Membership.GeneratePassword(8, 0);
          SendEmail(userName, "Jelszó emlékeztető",
            "Új jelszó: " + newPassword + " \n Ez a jelszó nem biztonságos, belépés után változtasd meg a jelszavad!");

          IdentityResult res = UserManager.RemovePassword(user.Id);
          IdentityResult res2 = UserManager.AddPassword(user.Id, newPassword);

          return new EmptyResult();
        }
        else
        {
          result.Errors.Add(new ValidationError("Nem regisztrált email cím!",3));
        }
      }

      Response.StatusCode = 403;
      return result.ToJsonNetResult();
    }




    [HttpPost]
    [AllowAnonymous]
    public ActionResult Register(RegisterViewModel model)
    {
      var res = new OperationResult<TabletLogin>();
      if (ModelState.IsValid)
      {
        var user = new ApplicationUser
        {
          UserName = model.UserName,
          Email = model.UserName,
          FirstName = model.FirstName,
          LastName = model.LastName
        };


        IdentityResult result = UserManager.Create(user, model.Password);
        if (result.Succeeded)
        {
          SignInAsync(user, false);

          _accountService.CreateRootFolder(user.Id);
          var t = TokenGenerator.GenerateToken(user.Id, user.PasswordHash, null);
          var r = new TabletLogin()
          {
            Token = t,
            UserName = user.UserName
          };
          res.Output = r;
          return res.ToJsonNetResult();
        }
        AddErrors(result);
        res.Errors.Add(new ValidationError(result.Errors.FirstOrDefault(), 5));
      }


      Response.StatusCode = 403;
      return res.ToJsonNetResult();
    }


    [System.Web.Http.HttpPost]
    [AuthorizeTablet]
    public ActionResult GetUserProfile()
    {
      var userId = HttpContext.Items["userId"] as string;

      return _accountService.GetApplicationUser(userId).ToJsonNetResult();
    }
    
   
    #region Helpers


    public void SendEmail(string address, string subject, string message)
    {
      string email = ConfigurationSettings.AppSettings["email"];
      string password = ConfigurationSettings.AppSettings["password"];

      var loginInfo = new NetworkCredential(email, password);
      var msg = new MailMessage();
      var smtpClient = new SmtpClient(ConfigurationSettings.AppSettings["smtpserver"], Convert.ToInt32(ConfigurationSettings.AppSettings["smtpport"]));

      msg.From = new MailAddress(email, ConfigurationSettings.AppSettings["name"]);
      msg.To.Add(new MailAddress(address));
      msg.Subject = subject;
      msg.Body = message;
      msg.IsBodyHtml = true;

      smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
      smtpClient.EnableSsl = true;
      smtpClient.UseDefaultCredentials = false;
      smtpClient.Credentials = loginInfo;
      smtpClient.Send(msg);
    }


    public class TabletLogin
    {
      [JsonProperty(PropertyName = "userName", NullValueHandling = NullValueHandling.Ignore)]
      public string UserName { get; set; }

      [JsonProperty(PropertyName = "token", NullValueHandling = NullValueHandling.Ignore)]
      public string Token { get; set; }

      [JsonProperty(PropertyName = "provider", NullValueHandling = NullValueHandling.Ignore)]
      public string Provider { get; set; }
    }


    public class RegisterViewModel
    {
      [Display(Name = "E-mail cím")]
      [Required(ErrorMessage = "Kötelező megadni az e-mail címet")]
      [EmailAddress(ErrorMessage = "Hibás e-mail cím formátum")]
      public string UserName { get; set; }


      [Required(ErrorMessage = "Kötelező megadni a vezetéknevet")]
      [Display(Name = "Vezetéknév")]
      public string LastName { get; set; }

      [Required(ErrorMessage = "Kötelező megadni a keresztnevet")]
      [Display(Name = "Keresztnév")]
      public string FirstName { get; set; }

      [Required(ErrorMessage = "Kötelező kitölteni a jelszó mezőt")]
      [StringLength(100, ErrorMessage = "Nem elég hosszú a jelszó.", MinimumLength = 6)]
      [DataType(DataType.Password)]
      [Display(Name = "Jelszó")]
      public string Password { get; set; }

      [DataType(DataType.Password)]
      [Display(Name = "Jelszó újra")]
      [System.Web.Mvc.Compare("Password", ErrorMessage = "Nem egyezik meg a jelszó mezővel")]
      public string ConfirmPassword { get; set; }
    }

    
    public enum ManageMessageId
    {
      ChangePasswordSuccess,
      SetPasswordSuccess,
      RemoveLoginSuccess,
      Error
    }

    private const string XsrfKey = "XsrfId";

    private IAuthenticationManager AuthenticationManager
    {
      get { return HttpContext.GetOwinContext().Authentication; }
    }

    private async Task SignInAsync(ApplicationUser user, bool isPersistent)
    {
      AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
      ClaimsIdentity identity =
        await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
      AuthenticationManager.SignIn(new AuthenticationProperties {IsPersistent = isPersistent}, identity);
    }

    private async Task SignIn(ApplicationUser user, bool isPersistent)
    {
      AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
      ClaimsIdentity identity =
        UserManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
      AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = isPersistent }, identity);
    }

    private void AddErrors(IdentityResult result)
    {
      foreach (string error in result.Errors)
      {
        ModelState.AddModelError("", error);
      }
    }

    private bool HasPassword()
    {
      ApplicationUser user = UserManager.FindById(User.Identity.GetUserId());
      if (user != null)
      {
        return user.PasswordHash != null;
      }
      return false;
    }

    private ActionResult RedirectToLocal(string returnUrl)
    {
      if (Url.IsLocalUrl(returnUrl))
      {
        return Redirect(returnUrl);
      }
      return RedirectToAction("Index", "Home");
    }

 

  

    private class ChallengeResult : HttpUnauthorizedResult
    {
      public ChallengeResult(string provider, string redirectUri)
        : this(provider, redirectUri, null)
      {
      }

      public ChallengeResult(string provider, string redirectUri, string userId)
      {
        LoginProvider = provider;
        RedirectUri = redirectUri;
        UserId = userId;
      }

      public string LoginProvider { get; set; }
      public string RedirectUri { get; set; }
      public string UserId { get; set; }

      public override void ExecuteResult(ControllerContext context)
      {
        var properties = new AuthenticationProperties {RedirectUri = RedirectUri};
        if (UserId != null)
        {
          properties.Dictionary[XsrfKey] = UserId;
        }
        context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
      }




    








    }

    #endregion
  }
}