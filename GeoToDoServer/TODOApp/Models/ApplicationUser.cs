﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace TODOApp.Models
{
  public class ApplicationUser : Microsoft.AspNet.Identity.EntityFramework.IdentityUser
  {
   

    public string Email { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }


    public int RootFolderId { get; set; }

  }
}