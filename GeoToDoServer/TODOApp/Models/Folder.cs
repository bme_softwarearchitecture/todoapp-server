﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;
using TODOApp.DTO;
using Newtonsoft.Json;

namespace TODOApp.Models
{
  [Table("Folder")]
  public class Folder
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }

    public Boolean IsDeleted { get; set; }

    public string Name { get; set; }
    public string Color { get; set; }
    public string UserId { get; set; }
    public int? ParentId { get; set; }
    public DateTime ChangedDate { get; set; }

    [ForeignKey("UserId")]
    public virtual ApplicationUser User { get; set; }

    public virtual ICollection<Note> Notes { get; set; }
    public virtual ICollection<Folder> Folders { get; set; }

    [ForeignKey("ParentId")]
    public virtual Folder Parent { get; set; }
  }
}