﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace TODOApp.Models
{
  [Table("Note")]
  public class Note
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }

    
    public string Title { get; set; }
    public string Content { get; set; }
    public string Place { get; set; }
    public DateTime CreatedDate { get; set; }
    public int ParentId { get; set; }
    public DateTime ChangedDate { get; set; }
    public string Color { get; set; }
    public string UserId { get; set; }

    public Boolean IsDeleted { get; set; }


    [ForeignKey("ParentId")]
    public virtual Folder Parent { get; set; }


    [ForeignKey("UserId")]
    public virtual ApplicationUser User { get; set; }
    
  }
}