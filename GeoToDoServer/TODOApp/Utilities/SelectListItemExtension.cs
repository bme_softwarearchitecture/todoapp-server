﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TODOApp.Utilities
{
  	public static class SelectListItemExtension
	{

		public static IEnumerable<SelectListItem> AddFirst(this IEnumerable<SelectListItem> enumerable, string value, string text)
		{
			var ret = new List<SelectListItem>
			{
				new SelectListItem {Text = text, Value = value}
			};
			ret.AddRange(enumerable);
			return ret;
		}

		public static SelectListItem SelectedOrFirst(this IEnumerable<SelectListItem> enumerable)
		{
			var selectListItems = enumerable as IList<SelectListItem> ?? enumerable.ToList();
			var first = selectListItems.FirstOrDefault(i => i.Selected);
			if (first != null)
			{
				return first;
			}
			return selectListItems.First();
		}
	}
  
}