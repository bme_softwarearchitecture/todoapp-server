﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using TODOApp.Service;

namespace TODOApp.Utilities
{
  public class HttpTabletBasicUnauthorizedResult : HttpUnauthorizedResult
  {
    public HttpTabletBasicUnauthorizedResult() : base() { }
    public HttpTabletBasicUnauthorizedResult(string statusDescription) : base(statusDescription) { }

    public override void ExecuteResult(ControllerContext context)
    {
      if (context == null) throw new ArgumentNullException("context");

      context.HttpContext.Response.AddHeader("WWW-Authenticate", "Basic");
      base.ExecuteResult(context);
    }
  }
   

}