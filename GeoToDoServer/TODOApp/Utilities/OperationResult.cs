﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using TODOApp.Service;
using Newtonsoft.Json;

namespace TODOApp.Utilities
{
  public class OperationResult
  {

    public bool IsSuccessful
    {
      get { return Errors == null || Errors.Count == 0; }
    }

    public JsonNetResult ToJsonNetResult()
    {
      JsonNetResult jsonNetResult = new JsonNetResult();
      jsonNetResult.Formatting = Newtonsoft.Json.Formatting.Indented;

      if (!IsSuccessful)
      {
        jsonNetResult.Data = GetFirstError();
        return jsonNetResult;
      }

      jsonNetResult.Data = "Ok";
      return jsonNetResult;

    }

    public List<ValidationError> Errors { get; set; }

    public OperationResult()
    {
      Errors = new List<ValidationError>();
    }

    public OperationResult(string errorMessage,int? errorCode)
      : this(new List<ValidationError> { new ValidationError(errorMessage,errorCode) })
    {
    }

    public OperationResult(List<ValidationError> errors)
    {
      Errors = errors;
    }

    public ValidationError GetFirstError()
    {
      if (Errors == null || !Errors.Any())
      {
        return new ValidationError("",-1);
      }

      return Errors.First();
    }

  }


  public class ValidationError
  {
    public ValidationError()
    {
    }

    public ValidationError(string errorMessage,int? errorCode)
    {
      ErrorMessage = errorMessage;
      ErrorCode = errorCode;
    }


    [JsonProperty(PropertyName = "errorMessage")]
    public string ErrorMessage { get; set; }

    [JsonProperty(PropertyName = "errorCode")]

    public int? ErrorCode { get; set; }
   

    
  }

  public class OperationResult<TOutput> : OperationResult
  {
    public TOutput Output { get; set; }

    public OperationResult()
      : base()
    {
    }

    public JsonNetResult ToJsonNetResult()
    {
      JsonNetResult jsonNetResult = new JsonNetResult();
      jsonNetResult.Formatting = Newtonsoft.Json.Formatting.Indented;

      if (!IsSuccessful)
      {
        jsonNetResult.Data = GetFirstError();
        return jsonNetResult;
      }

      jsonNetResult.Data = Output;
      return jsonNetResult;

    }

    public OperationResult(string errorMessage,int? errorCode, TOutput output)
      : this(new List<ValidationError> { new ValidationError( errorMessage,errorCode) }, output)
    {
    }

    public OperationResult(TOutput output)
      : this((List<ValidationError>)null, output)
    {
    }

    public OperationResult(List<ValidationError> errors, TOutput output)
      : base(errors)
    {
      Output = output;
    }
  }

   

}