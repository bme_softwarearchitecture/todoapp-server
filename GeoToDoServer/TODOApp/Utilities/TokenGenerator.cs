﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using TODOApp.Service;

namespace TODOApp.Utilities
{


  public static class TokenGenerator
  {
    private const int _expirationMinutes = 10;
    private const string _alg = "HmacSHA256";
    private const string _salt = "r9aLuOtFBXphj9WQfvFh";

    public static string GenerateToken(string username, string password, long? tick)
    {
      tick = 0;// tick != null ? tick : DateTime.UtcNow.Ticks;


      string hash = String.Join(":", new string[] { username, tick.ToString() });
      string hashLeft = "";
      string hashRight = "";

      using (HMAC hmac = HMACSHA256.Create(_alg))
      {
        hmac.Key = Encoding.UTF8.GetBytes((string) GetHashedPassword(password));
        hmac.ComputeHash(Encoding.UTF8.GetBytes(hash));

        hashLeft = Convert.ToBase64String(hmac.Hash);
        hashRight = String.Join(":", new string[] { username, tick.ToString() });
      }

      return Convert.ToBase64String(Encoding.UTF8.GetBytes(String.Join(":", hashLeft, hashRight)));
    }

    public static string GetHashedPassword(string password)
    {
      string key = String.Join(":", new string[] { password, _salt });

      using (HMAC hmac = HMACSHA256.Create(_alg))
      {
        // Hash the key.
        hmac.Key = Encoding.UTF8.GetBytes(_salt);
        hmac.ComputeHash(Encoding.UTF8.GetBytes(key));

        return Convert.ToBase64String(hmac.Hash);
      }
    }


    public static string GetUserName(string token, out long tick)
    {
      string result = null;

      try
      {
        // Base64 decode the string, obtaining the token:username:timeStamp.
        string key = Encoding.UTF8.GetString(Convert.FromBase64String(token));

        // Split the parts.
        string[] parts = key.Split(new char[] { ':' });
        long ticks = long.Parse(parts[2]);


        if (parts.Length == 3)
        {
          bool expired = false; //Math.Abs(TimeSpan.FromTicks((DateTime.UtcNow.Ticks - ticks)).TotalMinutes) > _expirationMinutes;

          if (expired)
          {
            tick = 0;
            return null;
          }

          // Get the hash message, username, and timestamp.
          string username = parts[1];

          tick = ticks;
          return username;


        }
      }
      catch
      {
      }

      tick = 0;
      return result;

    }
  }
   

}