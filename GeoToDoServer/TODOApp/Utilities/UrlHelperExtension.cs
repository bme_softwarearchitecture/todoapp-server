﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace TODOApp.Utilities
{
  public static class UrlHelperExtension
  {


    public static string Pager(this UrlHelper url, int pageNumber)
    {
      var rvd = new RouteValueDictionary(url.RequestContext.RouteData.Values);
      foreach (string key in url.RequestContext.HttpContext.Request.QueryString.Keys)
      {
        if (key != "loadPrevious")
        {
          rvd[key] = url.RequestContext.HttpContext.Request.QueryString[key];          
        }
      }
      rvd["pagenumber"] = pageNumber;

      return url.Action(rvd["action"].ToString(), rvd["controller"].ToString(), rvd);
    } 
  }
}