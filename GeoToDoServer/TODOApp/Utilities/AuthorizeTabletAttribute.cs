﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using TODOApp.Models;
using Facebook;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json.Linq;
using TODOApp.Service;

namespace TODOApp.Utilities
{
  public class AuthorizeTabletAttribute : AuthorizeAttribute
  {
    private readonly IAccountService _accountService;

   
    public AuthorizeTabletAttribute() : base()
    {
      _accountService = DependencyResolver.Current.GetService<IAccountService>();
    }

    protected override bool AuthorizeCore(HttpContextBase httpContext)
    {

      var authorization = httpContext.Request.Headers["Authorization"];
      var authorizationProvider = httpContext.Request.Headers["Authorization-Provider"];

      //nincsenek meg a szükséges paraméterek
      if (string.IsNullOrEmpty(authorization) || string.IsNullOrEmpty(authorizationProvider))
        return Unauthorized(httpContext);

      switch (authorizationProvider)
      {
        case "basic" :
          return ValidateToken(authorization,httpContext);
        default :
          return Unauthorized(httpContext);
      }
    }


    private bool Unauthorized(HttpContextBase httpContext)
    {
      httpContext.Response.Clear();
      httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
      httpContext.Response.End();
      return false;
    }



    private bool ValidateToken(string auth, HttpContextBase httpContext)
    {
      var token = auth.Split(' ');
      if (token.Count() != 2 || token[0] != "Bearer")
      {
        return Unauthorized(httpContext);
      }

      long tick;
      var userName = TokenGenerator.GetUserName(token[1], out tick);

      if (userName == null)
      {
        return Unauthorized(httpContext);
      }

      var user = _accountService.GetApplicationUser(userName).Output;

      if (user == null)
      {
        return Unauthorized(httpContext);
      }

      var newToken = TokenGenerator.GenerateToken(userName, user.PasswordHash, tick);

      if (token[1] != newToken)
      {
        return Unauthorized(httpContext);
      }

      httpContext.Items.Add("userId", user.Id); 
      
      return true;
    }
  }


}