﻿using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Microsoft.Practices.Unity;
using TODOApp.DI;
using TODOApp.DTO;
using TODOApp.Repository;
using TODOApp.Service;

namespace TODOApp
{
  public class MvcApplication : HttpApplication
  {
    protected void Application_Start()
    {
      AreaRegistration.RegisterAllAreas();
      GlobalConfiguration.Configure(WebApiConfig.Register);
      FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
      RouteConfig.RegisterRoutes(RouteTable.Routes);
      BundleConfig.RegisterBundles(BundleTable.Bundles);

      var mapper = AMapper.Instance;
      var container = new UnityContainer();
      container.RegisterType<IDBContext, DBContext>()
        .RegisterType<ITodoService, TodoService>()
        .RegisterType<IAccountService, AccountService>();
      DependencyResolver.SetResolver(new UnityDependencyResolver(container));
    }
  }
}