﻿using System;
using Newtonsoft.Json;

namespace TODOApp.DTO
{
  public class DTONote : DTOBase
  {
    [JsonProperty(PropertyName = "id")]
    public int? Id { get; set; }

    [JsonProperty(PropertyName = "title")]
    public string Title { get; set; }

    [JsonProperty(PropertyName = "content")]
    public string Content { get; set; }

    [JsonProperty(PropertyName = "place")]
    public string Place { get; set; }

    [JsonProperty(PropertyName = "createdDate")]
    public DateTime CreatedDate { get; set; }

    [JsonProperty(PropertyName = "parentId")]
    public int ParentId { get; set; }

    [JsonProperty(PropertyName = "changedDate")]
    public DateTime ChangedDate { get; set; }

    [JsonProperty(PropertyName = "color")]
    public string Color { get; set; }

    [JsonProperty(PropertyName = "userId")]
    public string UserId { get; set; }

    [JsonProperty(PropertyName = "isDeleted")]
    public Boolean IsDeleted { get; set; }
  }
}