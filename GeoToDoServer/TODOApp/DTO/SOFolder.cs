﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using TODOApp.Models;

namespace TODOApp.DTO
{
  public class SOFolder : SOBase<Folder>
  {
    public string UserId { get; set; }
    public string Name { get; set; }

    public int? Id { get; set; }

    public int? ParentId { get; set; }

    public DateTime? LasUpdate { get; set; }

    public SOFolder()
    {
      PageNumber = 0;
      PageSize = 20;
    }


    

  }
}