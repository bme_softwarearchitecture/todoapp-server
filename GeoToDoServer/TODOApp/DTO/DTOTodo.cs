﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using TODOApp.DTO;

namespace ErettsegiApp.DTO
{
  public class DTOTodo
  {
    [JsonProperty(PropertyName = "notes")]
    public List<DTONote> Notes { get; set; }

    [JsonProperty(PropertyName = "folders")]
    public List<DTOFolder> Folders { get; set; }

  }
}