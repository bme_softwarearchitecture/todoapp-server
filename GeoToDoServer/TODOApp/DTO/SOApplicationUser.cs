﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TODOApp.Models;

namespace TODOApp.DTO
{
  public class SOApplicationUser : SOBase<ApplicationUser>
  {
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string UserName { get; set; }
    public string UserId { get; set; }
    public string TeacherId { get; set; }
   

    public SOApplicationUser()
    {
      PageNumber = 0;
      PageSize = 10;
    }

  }
}