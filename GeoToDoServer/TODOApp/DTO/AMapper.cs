﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Microsoft.AspNet.Identity.EntityFramework;
using TODOApp.Models;

namespace TODOApp.DTO
{
  public interface IAMapper //: ISingletonDependency
  {
  }


  public class AMapper : IAMapper
  {
    private static AMapper instance;

    public static AMapper Instance
    {
      get
      {
        if (instance == null)
        {
          instance = new AMapper();
        }
        return instance;
      }
    }

    private AMapper()
    {
      Mapper.CreateMap<ApplicationUser, DTOApplicationUser>();
      
      Mapper.CreateMap<DTONote, Note>();
      Mapper.CreateMap<Note, DTONote>();

      Mapper.CreateMap<DTOFolder, Folder>();
      Mapper.CreateMap<Folder, DTOFolder>();
      
    }
  }
}