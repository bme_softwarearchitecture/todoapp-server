﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ErettsegiApp.DTO
{
  public class DTOSearchResult<So, Dto>
  {
    public So SearchOption { get; set; }
    public List<Dto> SearchResult { get; set; }
  }
}