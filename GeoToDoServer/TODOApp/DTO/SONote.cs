﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using TODOApp.Models;

namespace TODOApp.DTO
{
  public class SONote : SOBase<Note>
  {
    public string UserId { get; set; }

    public int? ParentId { get; set; }


    public int? NoteId { get; set; }

    public string SearchText { get; set; }

    public DateTime? LasUpdate { get; set; }


    public SONote()
    {
      PageNumber = 0;
      PageSize = 20;
    }


  }
}