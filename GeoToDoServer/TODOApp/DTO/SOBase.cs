﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace TODOApp.DTO
{
  public class SOBase<TObject> where TObject : class 
  {

    public int PageSize { get; set; }
    public int PageNumber { get; set; }
    public int PageCount { get; set; }

    public bool WithDeleted { get; set; }
    public bool DisablePageing { get; set; }
    public Expression<Func<TObject, bool>> SearchFilter { get; set; }
    public IQueryable<TObject> OrderFilter { get; set; }

  }
}