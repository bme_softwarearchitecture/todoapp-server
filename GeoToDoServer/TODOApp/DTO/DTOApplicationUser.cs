﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using TODOApp.Models;

namespace TODOApp.DTO
{
  public class DTOApplicationUser : DTOBase
  {

    [DisplayName("Keresztnév")]
    [JsonProperty(PropertyName = "firstName")]
    public string FirstName { get; set; }

    [DisplayName("Vezetéknév")]
    [JsonProperty(PropertyName = "lastName")]
    public string LastName { get; set; }

    [JsonProperty(PropertyName = "id")]
    public string Id { get; set; }


    [JsonProperty(PropertyName = "rootFolderId")]
    public int RootFolderId { get; set; }

    [DisplayName("Felhasználónév")]
    [JsonProperty(PropertyName = "userName")]
    public string UserName { get; set; }


    [JsonIgnore]
    public string PasswordHash { get; set; }


    [StringLength(100, ErrorMessage = "Nem elég hosszú a jelszó.", MinimumLength = 6)]
    [DataType(DataType.Password)]
    [Display(Name = "Jelszó")]
    [JsonIgnore]
    public string Password { get; set; }

    [DataType(DataType.Password)]
    [Display(Name = "Jelszó újra")]
    [System.Web.Mvc.Compare("Password", ErrorMessage = "Nem egyezik meg a jelszó mezővel")]
    [JsonIgnore]
    public string ConfirmPassword { get; set; }

  }
}