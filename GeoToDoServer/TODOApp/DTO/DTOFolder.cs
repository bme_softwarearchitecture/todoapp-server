﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace TODOApp.DTO
{
  public class DTOFolder : DTOBase
  {
    [JsonProperty(PropertyName = "id")]
    public int? Id { get; set; }

    [JsonProperty(PropertyName = "name")]
    public string Name { get; set; }

    [JsonProperty(PropertyName = "isDeleted")]
    public Boolean IsDeleted { get; set; }

    [JsonProperty(PropertyName = "color")]
    public string Color { get; set; }

    [JsonProperty(PropertyName = "userId")]
    public string UserId { get; set; }

    [JsonProperty(PropertyName = "parentId")]
    public int ParentId { get; set; }

    [JsonProperty(PropertyName = "changedDate")]
    public DateTime ChangedDate { get; set; }

    [JsonIgnore]
    public List<DTONote> Notes { get; set; }

    [JsonIgnore]
    public List<DTOFolder> Folders { get; set; }


    public bool IsValid()
    {
      var isValid = true;

      return isValid;
    }

    public static class ItemColor
    {
      public static string IndigoPink = "indigo_pink";
      public static string BlueIndigo = "blue_indigo";
      public static string TealAmber = "teal_amber";
      public static string CyanOrange = "cyan_orange";
      public static string LightBluePink = "light_blue_pink";
      public static string PurpleAmber = "purple_amber";
      public static string DeepPurpleOrange = "deep_purple_orange";
      public static string LightGreenAmber = "light_green_amber";
      public static string GreenPink = "green_pink";
      public static string AmberDeepOrange = "amber_deep_orange";
      public static string YellowOrange = "yellow_orange";
      public static string LimeTeal = "lime_teal";
      public static string OrangeDeepOrange = "orange_deep_orange";

      public static List<SelectListItem> getSelectedList()
      {
        var list = new List<SelectListItem>();


        list.Add(new SelectListItem
        {
          Text = "DeepPurpleOrange",
          Value = DeepPurpleOrange
        });
        list.Add(new SelectListItem
        {
          Text = "LightGreenAmber",
          Value = LightGreenAmber
        });

        list.Add(new SelectListItem
        {
          Text = "GreenPink",
          Value = GreenPink
        });
        list.Add(new SelectListItem
        {
          Text = "AmberDeepOrange",
          Value = AmberDeepOrange
        });
        list.Add(new SelectListItem
        {
          Text = "YellowOrange",
          Value = YellowOrange
        });
        list.Add(new SelectListItem
        {
          Text = "LimeTeal",
          Value = LimeTeal
        });
        list.Add(new SelectListItem
        {
          Text = "OrangeDeepOrange",
          Value = OrangeDeepOrange
        });
        list.Add(new SelectListItem
        {
          Text = "IndigoPink",
          Value = IndigoPink
        });

        list.Add(new SelectListItem
        {
          Text = "BlueIndigo",
          Value = BlueIndigo
        });

        list.Add(new SelectListItem
        {
          Text = "TealAmber",
          Value = TealAmber
        });

        list.Add(new SelectListItem
        {
          Text = "CyanOrange",
          Value = CyanOrange
        });
        list.Add(new SelectListItem
        {
          Text = "LightBluePink",
          Value = LightBluePink
        });
        list.Add(new SelectListItem
        {
          Text = "PurpleAmber",
          Value = PurpleAmber
        });


        return list;
      }
    }
  }
}