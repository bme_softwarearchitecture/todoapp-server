﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ErettsegiApp.DTO;
using TODOApp.DTO;
using TODOApp.Models;
using TODOApp.Repository;
using TODOApp.Utilities;

namespace TODOApp.Service
{
  public interface ITodoService
  {
    DTOSearchResult<SONote, DTONote> GetNotes(SONote so);
    DTOSearchResult<SOFolder, DTOFolder> GetFolders(SOFolder so);
    OperationResult<DTOTodo> SaveTodos(DTOTodo todo, string userId, string deviceId);
  }

  public class TodoService : ITodoService, IDisposable
  {
    private readonly IAccountService _accountService;
    private readonly IDBContext context;

    public TodoService(IDBContext dal, IAccountService accountService)
    {
      context = dal;
      _accountService = accountService;
    }

    public void Dispose()
    {
      if (context != null)
        context.Dispose();
    }

    public DTOSearchResult<SONote, DTONote> GetNotes(SONote so)
    {
      var result = new DTOSearchResult<SONote, DTONote>();
      result.SearchOption = so;

      so.SearchFilter =
        x => (!so.NoteId.HasValue || so.NoteId == x.Id) &&
             (string.IsNullOrEmpty(so.UserId) || so.UserId == x.UserId) &&
             (!so.ParentId.HasValue || so.ParentId == x.ParentId) &&
             (so.LasUpdate == null || so.LasUpdate < x.ChangedDate) &&
             (string.IsNullOrEmpty(so.SearchText) || x.Title.Contains(so.SearchText) ||
              x.Content.Contains(so.SearchText) || x.Place.Contains(so.SearchText));

      so.OrderFilter = context.NoteRepository.GetQueryable(so).OrderBy(x => x.Title);
      result.SearchResult = Enumerable.ToList(context.NoteRepository.Filter(so).Select(Mapper.Map<DTONote>));

      return result;
    }

    public DTOSearchResult<SOFolder, DTOFolder> GetFolders(SOFolder so)
    {
      var result = new DTOSearchResult<SOFolder, DTOFolder>();
      result.SearchOption = so;

      so.SearchFilter =
        x => (!so.Id.HasValue || so.Id == x.Id) &&
             (string.IsNullOrEmpty(so.UserId) || so.UserId == x.UserId) &&
             (!so.ParentId.HasValue || so.ParentId == x.ParentId) &&
             (so.LasUpdate == null || so.LasUpdate < x.ChangedDate) &&
             (string.IsNullOrEmpty(so.Name) || x.Name.Contains(so.Name));

      so.OrderFilter = context.FolderRepository.GetQueryable(so).OrderBy(x => x.Name);
      result.SearchResult = Enumerable.ToList(context.FolderRepository.Filter(so).Select(Mapper.Map<DTOFolder>));

      return result;
    }

    public OperationResult<DTOTodo> SaveTodos(DTOTodo todo, string userId, string deviceId)
    {
      var result = new OperationResult<DTOTodo>();
      result.Output = new DTOTodo();
      result.Output.Notes = new List<DTONote>();
      result.Output.Folders = new List<DTOFolder>();

      if (todo.Folders != null)
      {
        foreach (var folder in todo.Folders)
        {
          folder.UserId = userId;
          folder.ChangedDate = DateTime.Now;
          result.Output.Folders.Add(SaveFolder(folder));
        }
      }

      if (todo.Notes != null)
      {
        foreach (var note in todo.Notes)
        {
          note.UserId = userId;
          note.ChangedDate = DateTime.Now;
          result.Output.Notes.Add(SaveNote(note));
        }
      }

      _accountService.SendUpdateNotification(userId, deviceId);
      return result;
    }

    private DTONote SaveNote(DTONote note)
    {
      if (note.Id.HasValue)
        note = Mapper.Map<DTONote>(context.NoteRepository.Update(Mapper.Map<Note>(note), x => x.Id == note.Id));
      else
        note = Mapper.Map<DTONote>(context.NoteRepository.Create(Mapper.Map<Note>(note)));

      return note;
    }

    private DTOFolder SaveFolder(DTOFolder folder)
    {
      if (folder.Id.HasValue)
        folder =
          Mapper.Map<DTOFolder>(context.FolderRepository.Update(Mapper.Map<Folder>(folder), x => x.Id == folder.Id));
      else
        folder = Mapper.Map<DTOFolder>(context.FolderRepository.Create(Mapper.Map<Folder>(folder)));

      return folder;
    }
  }
}