﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using DotNet.Highcharts.Options;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity.EntityFramework;
using PushSharp;
using PushSharp.Android;
using TODOApp.DTO;
using TODOApp.Models;
using TODOApp.Repository;
using TODOApp.Utilities;

namespace TODOApp.Service
{
  public interface IAccountService
  {
    void CreateRootFolder(string userId);
    void UpdateDeviceId(string userId,string oldId, string newId);
    void SendUpdateNotification(string id,string deviceId);
    OperationResult<DTOApplicationUser> GetApplicationUser(string id);

  }

  public class AccountService : IAccountService, IDisposable
  {
    private IDBContext context;
    
    public AccountService(IDBContext dal)
    {
      context = dal;
    }

    
    public void Dispose()
    {
      if (context != null)
        context.Dispose();
    }




    public void CreateRootFolder(string userId)
    {
      var folder = new Folder()
      {
        IsDeleted = false,
        Color = null,
        ChangedDate = DateTime.Now,
        Name = "Root",
        UserId = userId,
        ParentId = null,
      };

      var rootFolder = Mapper.Map<DTOFolder>(context.FolderRepository.Create(folder));

      var user = context.ApplicationUser.Find(x => x.Id == userId);
      user.RootFolderId = rootFolder.Id.Value;

      context.ApplicationUser.Update(user,x=> x.Id==userId);
    }

    public void UpdateDeviceId(string userId, string oldId, string newId)
    {
      var old = context.UserLoginRepository.Find(x => x.UserId == userId && x.ProviderKey == oldId);
      if (old!=null)
        context.UserLoginRepository.Delete(old);

      var newDevice = context.UserLoginRepository.Find(x => x.UserId == userId && x.ProviderKey == newId);
      if (newDevice == null && !string.IsNullOrEmpty(userId) && !string.IsNullOrEmpty(newId))
      {
        context.UserLoginRepository.Create(new IdentityUserLogin()
        {
          UserId = userId,
          ProviderKey = newId,
          LoginProvider = "Android"
        });
      }
    }

    public void SendUpdateNotification(string id,string deviceId)
    {
      var push = new PushBroker();
      push.RegisterGcmService(new GcmPushChannelSettings("AIzaSyBtQYiJgXyQcjk0HOy5rVgHTZokulcnBAA"));

      var deviceIds = context.UserLoginRepository.Filter(x => x.UserId == id && x.ProviderKey != deviceId)
        .Select(x => x.ProviderKey)
        .ToList();

      push.QueueNotification(new GcmNotification().ForDeviceRegistrationId(deviceIds)
                      .WithJson("{\"alert\":\"Hello World!\",\"badge\":7,\"sound\":\"sound.caf\"}"));
    }

    public OperationResult<DTOApplicationUser> GetApplicationUser(string id)
    {
      var result = new OperationResult<DTOApplicationUser>();
      var appUser = context.ApplicationUser.Find(x => x.Id == id);
      context.Reload(appUser);
      result.Output = Mapper.Map<DTOApplicationUser>(appUser);

      return result;
    }

 
 
 

    
  }
}